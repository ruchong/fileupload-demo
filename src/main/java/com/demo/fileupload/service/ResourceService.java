package com.demo.fileupload.service;

import com.demo.fileupload.entity.Resource;
import com.demo.fileupload.vo.FileSaveInfo;

/**
 * Created by lixiaoxin on 2017/12/15.
 */
public interface ResourceService extends BaseService{
    void saveUpload( Resource resource, FileSaveInfo info);
    void saveUpload( Resource resource);
}
