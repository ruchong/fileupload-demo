package com.demo.fileupload.dto;

import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by lixiaoxin on 2017/12/22.
 */
public class ResponseImageUriPageList extends ResponsePageList {
    private String imageUri;

    public ResponseImageUriPageList(){

    }
    public ResponseImageUriPageList(List<Object> list, ResponsePage pages, String totalCount) {
        setList(list);
        setPages(pages);
        setTotalCount(totalCount);
    }

    public ResponseImageUriPageList(List<Object> list, ResponsePage pages) {
        setList(list);
        setPages(pages);
        setTotalCount(getPages().getCount() + "");
    }

    public ResponseImageUriPageList(Page page,String imageUri) {
        setList(page.getContent());
        ResponsePage res = new ResponsePage();
        res.setPer_page(page.getSize());
        res.setPage_count(page.getTotalPages());
        res.setCurrent_page(page.getNumber() + 1);
        res.setCount(page.getTotalElements());
        setPages(res);
        setTotalCount(getPages().getCount() + "");
        this.imageUri = imageUri;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }
}
