package com.demo.fileupload.dto;

import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by lixiaoxin on 2017/12/6.
 */
public class ResponsePageList {
    private List<Object> list;
    private ResponsePage pages;
    private String totalCount;

    public ResponsePageList() {
    }

    public ResponsePageList(List<Object> list, ResponsePage pages, String totalCount) {
        this.list = list;
        this.pages = pages;
        this.totalCount = totalCount;
    }

    public ResponsePageList(List<Object> list, ResponsePage pages) {
        this.list = list;
        this.pages = pages;
        this.totalCount = this.pages.getCount() + "";
    }

    public ResponsePageList(Page page) {
        this.list = page.getContent();
        ResponsePage res = new ResponsePage();
        res.setPer_page(page.getSize());
        res.setPage_count(page.getTotalPages());
        res.setCurrent_page(page.getNumber() + 1);
        res.setCount(page.getTotalElements());
        this.pages = res;
        this.totalCount = this.pages.getCount() + "";
    }


    public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    public ResponsePage getPages() {
        return pages;
    }

    public void setPages(ResponsePage pages) {
        this.pages = pages;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }
}
