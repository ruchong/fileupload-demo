package com.demo.fileupload.repository;

import com.demo.fileupload.entity.CityArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lixiaoxin on 2017/12/13.
 */
@Repository
public interface CityAreaRepository extends JpaRepository<CityArea,Long> {
    List<CityArea> findByPrevCodeOrderByCode(String prevCode);

    CityArea getByCode(String code);

    CityArea getById(Long id);
}
